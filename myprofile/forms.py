from django import forms

class Form10(forms.Form):
    error_messages = {
        'required': 'Please fill in the input',
    }
    email = forms.EmailField(required=True, widget=forms.EmailInput(attrs={'class':'form-control','placeholder':'Email', 'id':'email'}))
    name = forms.CharField(required=True, max_length=100,  widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Name', 'id':'name'}))
    password = forms.CharField(required=True, max_length=50, widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':'Password', 'id':'password'}))