from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, add_subs, validate_email
from .models import Subscribe

class Story08Test(TestCase):
    def test_story08_url_is_exist(self):
        response = Client().get('/myprofile/')
        self.assertEqual(response.status_code,200)

    def test_story08_using_index_func(self):
        found = resolve('/myprofile/')
        self.assertEqual(found.func, index)

    def test_title_profile(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Story 08 - Jeffrey's Profile", html_response)

    def models_can_create_new_subscriber(self):
        Subscribe.objects.create(email='hehe@gmail.com', name='hehe', password='123')
        count = Subscribe.objects.all().count()
        self.assertEqual(count, 1)

    def test_story10_post_success_and_render_the_result(self):
        test = 'anonymous'
        response_post = Client().post('/myprofile/subscribe', {'email': test})
        self.assertEqual(response_post.status_code, 301)

    def test_story06_post_error_and_render_the_result(self):
        test = 'yolo'
        response_post = Client().post('/myprofile/subscribe', {'status': ''})
        self.assertEqual(response_post.status_code, 301)

        response= Client().get('/myprofile/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
    
    def test_story10_can_check_email(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST = {'email' : "ahoy@ui.com" , 'name' : 'Ahoy', 'password' : 'ahoylah'}
        response = add_subs(request)
        request.POST = {'email' : 'ahoy'}
        response2 = validate_email(request)
        self.assertJSONEqual(str(response2.content, encoding='utf8'), {'is_taken':False})

