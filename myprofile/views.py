from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from .forms import Form10
from .models import Subscribe

# Create your views here.

def load(request):
    return render(request, 'story08/loading.html',{})

response = {}
def index(request):    
    response['author'] = "Jeffrey" 
    subs = Subscribe.objects.all()
    response['subs'] = subs
    html = 'story08/index.html'
    response['form10'] = Form10
    return render(request, html, response)

def add_subs(request):
    form = Form10(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['email'] = request.POST['email']
        response['name'] = request.POST['name']
        response['password'] = request.POST['password']
        subs = Subscribe(email=response['email'],name=response['name'], password=response['password'])
        subs.save()
        return HttpResponseRedirect('/myprofile/')
    else:
        return HttpResponseRedirect('/myprofile/')

@csrf_exempt
def validate_email(request):
    email = request.POST.get('email', None)
    data = {
        'is_taken': Subscribe.objects.filter(email=email).exists()
    }
    return JsonResponse(data)
