from django.db import models

# Create your models here.
class Subscribe(models.Model):
    email= models.EmailField(unique=True)
    name =models.CharField(max_length=100)
    password = models.CharField(max_length=50)
