var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}

$(document).ready(function() {
    $('.my-select').select2();
});

localStorage.theme = '['+
  '{"id":0,"text":"Light","bcgColor":"#FFFFFF","fontColor":"#212121"},'+
  '{"id":1,"text":"Dark","bcgColor":"#212121","fontColor":"#FFFFFF"}]';

$(document).ready(function(){
  $('.my-select').select2({
    "data" : JSON.parse(localStorage.theme)
  });

  if (localStorage.getItem("selectedTheme") == null) {
    localStorage.selectedTheme = JSON.stringify(JSON.parse(localStorage.theme)[0]);
    theme = JSON.parse(localStorage.selectedTheme);
    changeBGColor(theme.bcgColor, theme.fontColor);
  }
  else{
    theme = JSON.parse(localStorage.selectedTheme);
    changeBGColor(theme.bcgColor, theme.fontColor);
  }
});

function changeBGColor(bg, fontcolor){
  $('body').css('background', bg);
  $('body').css('color' , fontcolor);
}

$('.apply-button').on('click', function(){
    // [TODO] ambil value dari elemen select .my-select
    var no = $('.my-select').val();
    var theme = JSON.parse(localStorage.theme)

    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada 
    // [TODO] ambil object theme yang dipilih
    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    // [TODO] simpan object theme tadi ke local storage selectedTheme

    for (i = 0;i<theme.length;i++){
        if (no == theme[i].id){
        var selected = theme[i];
        changeBGColor(selected.bcgColor, selected.fontColor);
        localStorage.selectedTheme = JSON.stringify(theme[i]);
        break;
        }
    }
  })

//Story 10
function checkform() {
    var f = document.forms["form"].elements;
    var cansubmit = true;

    for (var i = 1; i < f.length; i++) {
        if (f[i].value == "")
            cansubmit = false;
    }

    document.getElementById('subscribe').disabled = !cansubmit;
}
window.onkeydown = checkform;

$("#email").change(function () {
    console.log( $(this).val() );
    email = $(this).val();
    $.ajax({
        method: "POST",
        url: "https://ppw-story08.herokuapp.com/myprofile/check/",
        data: {
            'email': email
        },
        dataType: 'json',
        success: function (data) {
            console.log(data)
            if (data.is_taken) {
                alert("Email seperti itu sudah ada");
            }
        }
    });
});
