from django.urls import path
from . import views

urlpatterns= [
	path('',views.index,name='index'), 
	path('loading/', views.load, name='load'),
	path('subscribe/', views.add_subs, name='subscribe'),
	path('check/', views.validate_email, name='check')

]
